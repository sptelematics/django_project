import logging
import socket
import sys
import traceback
import datetime
import time

from configparser import ConfigParser
from protocol.src.com.teltonika.TCPlistener.tcp_logger import *


class tcp_client:
    'This is the TCP client class for sending the Teltonika messages to tcp_server'

    def __init__(self):
        self.name = self

    @staticmethod
    def read_config(self):
        # read the config file and get the values
        tcp_config = ConfigParser()
        tcp_config.read('config.ini')
        tcp_host = tcp_config['socket_cfg']['host']
        tcp_port = tcp_config['socket_cfg']['port']
        print("Host : ", tcp_host, " Port : ", tcp_port)
        return tcp_host, tcp_port

    @staticmethod
    def call_logger(self):
        print('before calling the logger')
        log_file_name = __file__
        print('I am here  ', __file__, ' sliced : ', log_file_name[2:])
        my1_log = log_file_name[2:]

        # create the logger and get the handle
        cl_logger = tcp_logger(my1_log)
        tcp_log_info = cl_logger.create_logger()
        tcp_log_info('Starting the logger')
        return tcp_log_info

    def run_client(self):

        # start the logger
        cl_log = tcp_client.call_logger(self)

        # read the config file
        tcp_port, tcp_host = tcp_client.read_config(self)

        # create TCP IP socket
        try:
            soc_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            soc_tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            cl_log("Socket successfully created")
        except socket.error as err:
            print("Socket creation failed with error %s" % err)
            sys.exit()

        # Try to bind the socket and port
        try:
            soc_tcp.connect((tcp_host, tcp_port))
            cl_log("Binding to host and port ")
        except:
            print("bind failed. Error : " + str(sys.exc_info()))
            sys.exit()

        # open the file Teltonika.msg and read the contents
        # send each line from client to server as utf8 format
        # for each line sent wait for am ACK = 01 from server
        # when ACK = 01 is received from server send the next line.
        # write the sent string in the logger file
        # when end of file reaches after the last ACK close the file and connection
        # Ankush to do this
        with open('received_file', 'wb') as f:
            print('file opened')
            while True:
                print('receiving data...')
                data = soc_tcp.send(1024)
                print('data=%s', (data))
                if not data:
                    break
                # write data to a file
                f.write(data)

        f.close()
        print('Successfully get the file')
        soc_tcp.close()
        print('connection closed')