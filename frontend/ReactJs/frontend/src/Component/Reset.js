import React, { Component } from "react";
import { render } from "react-dom";
import LoginService from '../service/PostData';
const loginService = new LoginService();
class Reset extends Component {
  constructor(props) {
    super(props);
  this.submitData = this.submitData.bind(this);
  } handleUpdate(){
        loginService.reset(
    {
      password: "",
      confirm_password: "",
      token: "",
      uidb64: "",

    });
  }
  inputPassword = event => {
    this.setState({ password: event.target.value });
  };
  confirmPassword = event => {
    this.setState({ confirm_password: event.target.value });
  };
  tokenPassword = event => {
    this.setState({ token: event.target.value });
  };
  uidb64Password = event => {
    this.setState({ uidb64: event.target.value });
  };
  submitData(event) {
    const { password, confirm_password } = this.state;
    const matches = password === confirm_password;
     {
          this.handleUpdate();
        }
  event.preventDefault();
    matches ? alert("password match") : alert("NO MATCH");
  }
  render() {
    return (
      <div>
        <center><img src = {process.env.PUBLIC_URL + '/logo.svg' } alt="Logo" className="logo" /><br/></center>
            <h4 className="forgetpwd">Recovery Email example@gmail.com</h4>
        <form onSubmit={this.submitData}>
                    <div className="form-group">
                       <input type="password" className="form-control"  name="password" placeholder="Enter password" onChange={this.inputPassword} />
                    </div>
                    <div className="form-group">
                       <input type="password"  name="confirm_password" className="form-control" placeholder="Confirm password"  onChange={this.confirmPassword} />
                    </div>
                    <div className="form-group">
                       <input type="text"  name="token" className="form-control" placeholder="Token"  onChange={this.tokenPassword} />
                    </div>
                    <div className="form-group">
                       <input type="text"  name="uidb64" className="form-control" placeholder="uidb64"  onChange={this.uidb64Password} />
                    </div>
                    <button className="btn btn-primary" type="submit">Send</button>
        </form>
      </div>
    );
  }
}
export default Reset;