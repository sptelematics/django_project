from django.shortcuts import render
from rest_framework import generics
from .serializers import tblcountrySerializer, tblresellerSerializer, tblurlSerializer, \
    tbluserSerializer, tblvehicleSerializer, tblpassengerSerializer, tblvehicletypeSerializer, \
    tbltrailerSerializer, tbldriverSerializer, tblcompanySerializer, tbladdressSerializer, tblgroupSerializer
from .models import (tblcountries, tblreseller, tblurl,
                     tbluser, tblvehicle, tblpassengers, tblvehicletype,
                     tbltrailers, tbldriver, tblcompany, tbladdress,
                     tblgroup)
from rest_framework.authentication import BaseAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated


# Create your views here.
def home(request):
    urldata = tblurl.objects.all()
    print(tblurl)
    context = {'data': urldata}
    return render(request, 'GPS/home.html', context)


# first api for countries
class tblCountry_CR_View(generics.ListCreateAPIView):
    queryset = tblcountries.objects.all()
    serializer_class = tblcountrySerializer


class tblCountry_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblcountries.objects.all()
    serializer_class = tblcountrySerializer


# 2nd  api for address
class tbladdress_CR_View(generics.ListCreateAPIView):
    queryset = tbladdress.objects.all()
    serializer_class = tbladdressSerializer


class tbladdress_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tbladdress.objects.all()
    serializer_class = tbladdressSerializer


# 3rd  api for url
class tblurl_CR_View(generics.ListCreateAPIView):
    queryset = tblurl.objects.all()
    serializer_class = tblurlSerializer


class tblurl_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblurl.objects.all()
    serializer_class = tblurlSerializer


# 4th  api for reseller
class tblReseller_CR_View(generics.ListCreateAPIView):
    queryset = tblreseller.objects.all()
    serializer_class = tblresellerSerializer


class tblReseller_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblreseller.objects.all()
    serializer_class = tblresellerSerializer


# 5th api for company
class tblcompany_CR_View(generics.ListCreateAPIView):
    queryset = tblcompany.objects.all()
    serializer_class = tblcompanySerializer


class tblcompany_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblcompany.objects.all()
    serializer_class = tblcompanySerializer


# 6th api for tbluser
class tbluser_CR_view(generics.ListCreateAPIView):
    queryset = tbluser.objects.all()
    serializer_class = tbluserSerializer


class tbluser_RU_view(generics.RetrieveUpdateAPIView):
    queryset = tbluser.objects.all()
    serializer_class = tbluserSerializer


# 7th api for tblvehicletype
class tblvehicletype_CR_View(generics.ListCreateAPIView):
    queryset = tblvehicletype.objects.all()
    serializer_class = tblvehicletypeSerializer


class tblvehicletype_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblvehicletype.objects.all()
    serializer_class = tblvehicletypeSerializer


# 8th api for tblgroup
class tblgroup_CR_View(generics.ListCreateAPIView):
    queryset = tblgroup.objects.all()
    serializer_class = tblgroupSerializer


class tblgroup_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblgroup.objects.all()
    serializer_class = tblgroupSerializer


# 9th api for tbldriver
class tbldriver_CR_View(generics.ListCreateAPIView):
    queryset = tbldriver.objects.all()
    serializer_class = tbldriverSerializer


class tbldriver_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tbldriver.objects.all()
    serializer_class = tbldriverSerializer


# 10th api for passenger
class tblpassenger_CR_View(generics.ListCreateAPIView):
    queryset = tblpassengers.objects.all()
    serializer_class = tblpassengerSerializer


class tblpassenger_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblpassengers.objects.all()
    serializer_class = tblpassengerSerializer


# 11th api for tbltrailers
class tbltrailers_CR_View(generics.ListCreateAPIView):
    queryset = tbltrailers.objects.all()
    serializer_class = tbltrailerSerializer


class tbltrailers_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tbltrailers.objects.all()
    serializer_class = tbltrailerSerializer


# 12th api for tbltrailers
class tblvehicle_CR_View(generics.ListCreateAPIView):
    queryset = tblvehicle.objects.all()
    serializer_class = tblvehicleSerializer


class tblvehicle_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblvehicle.objects.all()
    serializer_class = tblvehicleSerializer
