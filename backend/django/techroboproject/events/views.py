from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from rest_framework import generics

from .serializers import (MainSerializer, TimeSerializer, NotificationsSerializer)
from .models import (tblMain, tblTime, tblNotifications)


# Create your views here.

class Main_CR_View(generics.ListCreateAPIView):
    queryset = tblMain.objects.all()
    serializer_class = MainSerializer

class Main_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblMain.objects.all()
    serializer_class = MainSerializer


class Time_CR_View(generics.ListCreateAPIView):
    queryset = tblTime.objects.all()
    serializer_class = TimeSerializer


class Time_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblTime.objects.all()
    serializer_class = TimeSerializer


class Notifications_CR_View(generics.ListCreateAPIView):
    queryset = tblNotifications.objects.all()
    serializer_class = NotificationsSerializer


class Notifications_RU_View(generics.RetrieveUpdateAPIView):
    queryset = tblNotifications.objects.all()
    serializer_class = NotificationsSerializer
