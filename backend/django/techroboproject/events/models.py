from django.db import models


# Create your models here.
from GPS.models import tblvehicletype


class tblMain(models.Model):
    types = (
        ('overspeed', 'overspeed'),
        ('moving', 'moving'),
        ('underspeed', 'underspeed'),
        ('stopped', 'stopped'),
        ('route_in', 'route_in'),
        ('route_out', 'route_out'),
        ('zone_in', 'zone_in'),
        ('harsh braking', 'harsh braking'),
        ('driver change', 'drive change'),
    )

    depending_on_routes = (
        ('off', 'off'),
        ('In selected routes', 'In selected routes'),
        ('Out of selected routes', 'Out of selected routes')

    )

    depending_on_zones = (
        ('off', 'off'),
        ('In selected zones', 'In selected zones'),
        ('Out of selected zones', 'Out of selected zones')

    )
    zones = (
        ('Bialystok', 'Bialystok'),
        ('Coventry', 'Coventry'),
        ('Goole', 'Goole'),
        ('Heywood', 'Heywood'),
        ('Ostrow', 'Ostrow'),
        ('Preston', 'Preston'),
        ('Veino', 'Veino'),
        ('Warsaw', 'Warsaw')
    )
    Active = models.BooleanField()
    Name = models.CharField(max_length=100)
    Type = models.CharField(max_length=100, choices=types)
    TblVehicleType = models.ForeignKey(tblvehicletype, on_delete=models.CASCADE)
    Depending_on_routes = models.CharField(max_length=100, choices=depending_on_routes)
    Depending_on_zones = models.CharField(max_length=100, choices=depending_on_zones)
    Zones = models.CharField(max_length=100, choices=zones)
    Time_period = models.TimeField()
    speed_limit = models.IntegerField()

    def __str__(self):
        return self.Name


class tblTime(models.Model):
    week_days = (
        ('MON', 'Monday'),
        ('TUE', 'Tuesday'),
        ('WED', 'Wednesday'),
        ('THU', 'Thursday'),
        ('FRI', 'Friday'),
        ('SAT', 'Saturday'),
        ('SUN', 'Sunday')
    )

    Duration_form_last_event_in_minutes = models.IntegerField()
    Week_days = models.CharField(max_length=100, choices=week_days)
    Day_time = models.BooleanField()


class tblNotifications(models.Model):
    sound_alert = (
        ('alarm1.mp3', 'alarm1.mp3'),
        ('alarm2.mp3', 'alarm2.mp3'),
        ('alarm3.mp3', 'alarm3.mp3'),
        ('alarm4.mp3', 'alarm4.mp3')
    )
    email_template = (('Default', 'Default'),)
    sms_template = (('Default', 'Default'),)
    object_arrow_color = (
        ('Yellow', 'Yellow'),
        ('Black', 'Black'),
        ('Blue', 'Blue'),
        ('Green', 'Green'),
        ('Purple', 'Purple')
    )

    System_message = models.BooleanField()
    Auto_hide = models.BooleanField()
    Push_Notification = models.BooleanField()
    Sound_alert = models.BooleanField(max_length=100, choices=sound_alert)
    Message_to_email_for_multiple_emails_separate_them_by_comma = models.EmailField(max_length=200, unique=True)
    SMS_to_mobile_phone_for_multiple_phone_numbers_separate_them_by_comma = models.IntegerField()
    E_mail_template = models.CharField(max_length=100, choices=email_template)
    SMS_template = models.CharField(max_length=100, choices=sms_template)
    Object_arrow_color = models.CharField(max_length=100, choices=object_arrow_color)
