from rest_framework import serializers
from .models import (tblMain, tblTime, tblNotifications)


class MainSerializer(serializers.ModelSerializer):
    class Meta:
        model = tblMain
        fields = '__all__'


class TimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = tblTime
        fields = '__all__'


class NotificationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = tblNotifications
        fields = '__all__'


