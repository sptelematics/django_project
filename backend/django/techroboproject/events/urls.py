from django.urls import path

from events import views

urlpatterns = [
    #path('',views.EventView.as_view,name='event')
    path('main/',views.Main_CR_View.as_view()),
    path('main/<int:pk>/',views.Main_RU_View.as_view()),
    path('time/', views.Time_CR_View.as_view()),
    path('time/<int:pk>/', views.Time_RU_View.as_view()),
    path('notification/', views.Notifications_CR_View.as_view()),
    path('notification/<int:pk>/', views.Notifications_RU_View.as_view()),
]