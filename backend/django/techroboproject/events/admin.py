from django.contrib import admin
from .models import tblMain,tblTime, tblNotifications
# Register your models here.

admin.site.register(tblMain,)
admin.site.register(tblTime,)
admin.site.register(tblNotifications,)